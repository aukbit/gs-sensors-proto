// Code generated by protoc-gen-go.
// source: sensors.proto
// DO NOT EDIT!

/*
Package sensor is a generated protocol buffer package.

It is generated from these files:
	sensors.proto

It has these top-level messages:
	Serial
	Sensor
*/
package sensor

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

import (
	context "golang.org/x/net/context"
	grpc "google.golang.org/grpc"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type Sensor_Type int32

const (
	Sensor_DEFAULT        Sensor_Type = 0
	Sensor_TEMPERATURE    Sensor_Type = 1
	Sensor_PHOTORESISTURE Sensor_Type = 2
	Sensor_MOISTURE       Sensor_Type = 3
)

var Sensor_Type_name = map[int32]string{
	0: "DEFAULT",
	1: "TEMPERATURE",
	2: "PHOTORESISTURE",
	3: "MOISTURE",
}
var Sensor_Type_value = map[string]int32{
	"DEFAULT":        0,
	"TEMPERATURE":    1,
	"PHOTORESISTURE": 2,
	"MOISTURE":       3,
}

func (x Sensor_Type) String() string {
	return proto.EnumName(Sensor_Type_name, int32(x))
}
func (Sensor_Type) EnumDescriptor() ([]byte, []int) { return fileDescriptor0, []int{1, 0} }

type Serial struct {
	Serial string `protobuf:"bytes,1,opt,name=serial" json:"serial,omitempty"`
}

func (m *Serial) Reset()                    { *m = Serial{} }
func (m *Serial) String() string            { return proto.CompactTextString(m) }
func (*Serial) ProtoMessage()               {}
func (*Serial) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

// The message containing the sensor data
type Sensor struct {
	Id       int64             `protobuf:"varint,1,opt,name=id" json:"id,omitempty"`
	Serial   string            `protobuf:"bytes,2,opt,name=serial" json:"serial,omitempty"`
	Type     Sensor_Type       `protobuf:"varint,3,opt,name=type,enum=sensor.Sensor_Type" json:"type,omitempty"`
	Name     string            `protobuf:"bytes,4,opt,name=name" json:"name,omitempty"`
	Location string            `protobuf:"bytes,5,opt,name=location" json:"location,omitempty"`
	Metadata map[string]string `protobuf:"bytes,6,rep,name=metadata" json:"metadata,omitempty" protobuf_key:"bytes,1,opt,name=key" protobuf_val:"bytes,2,opt,name=value"`
}

func (m *Sensor) Reset()                    { *m = Sensor{} }
func (m *Sensor) String() string            { return proto.CompactTextString(m) }
func (*Sensor) ProtoMessage()               {}
func (*Sensor) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

func (m *Sensor) GetMetadata() map[string]string {
	if m != nil {
		return m.Metadata
	}
	return nil
}

func init() {
	proto.RegisterType((*Serial)(nil), "sensor.Serial")
	proto.RegisterType((*Sensor)(nil), "sensor.Sensor")
	proto.RegisterEnum("sensor.Sensor_Type", Sensor_Type_name, Sensor_Type_value)
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// Client API for Querier service

type QuerierClient interface {
	GetSensorBySerial(ctx context.Context, in *Serial, opts ...grpc.CallOption) (*Sensor, error)
}

type querierClient struct {
	cc *grpc.ClientConn
}

func NewQuerierClient(cc *grpc.ClientConn) QuerierClient {
	return &querierClient{cc}
}

func (c *querierClient) GetSensorBySerial(ctx context.Context, in *Serial, opts ...grpc.CallOption) (*Sensor, error) {
	out := new(Sensor)
	err := grpc.Invoke(ctx, "/sensor.Querier/GetSensorBySerial", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Server API for Querier service

type QuerierServer interface {
	GetSensorBySerial(context.Context, *Serial) (*Sensor, error)
}

func RegisterQuerierServer(s *grpc.Server, srv QuerierServer) {
	s.RegisterService(&_Querier_serviceDesc, srv)
}

func _Querier_GetSensorBySerial_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Serial)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(QuerierServer).GetSensorBySerial(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/sensor.Querier/GetSensorBySerial",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(QuerierServer).GetSensorBySerial(ctx, req.(*Serial))
	}
	return interceptor(ctx, in, info, handler)
}

var _Querier_serviceDesc = grpc.ServiceDesc{
	ServiceName: "sensor.Querier",
	HandlerType: (*QuerierServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetSensorBySerial",
			Handler:    _Querier_GetSensorBySerial_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "sensors.proto",
}

func init() { proto.RegisterFile("sensors.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 313 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x09, 0x6e, 0x88, 0x02, 0xff, 0x5c, 0x91, 0xc1, 0x4e, 0xfa, 0x40,
	0x10, 0xc6, 0xe9, 0xb6, 0x14, 0xfe, 0xc3, 0x9f, 0x8a, 0xa3, 0x31, 0x0d, 0xf1, 0xd0, 0xf4, 0x22,
	0xa7, 0x1e, 0x30, 0x26, 0x44, 0x2f, 0x62, 0x5c, 0xd4, 0x44, 0x02, 0x2e, 0xe5, 0x01, 0x56, 0xd9,
	0x43, 0x63, 0x69, 0x9b, 0xed, 0x62, 0xd2, 0x37, 0xf4, 0xb1, 0x4c, 0xb7, 0x1b, 0x04, 0x6f, 0xf3,
	0xdb, 0xf9, 0xbe, 0xd9, 0xf9, 0x32, 0xd0, 0x2f, 0x45, 0x56, 0xe6, 0xb2, 0x8c, 0x0a, 0x99, 0xab,
	0x1c, 0xdd, 0x06, 0xc3, 0x00, 0xdc, 0x95, 0x90, 0x09, 0x4f, 0xf1, 0x02, 0xdc, 0x52, 0x57, 0xbe,
	0x15, 0x58, 0xa3, 0x7f, 0xcc, 0x50, 0xf8, 0x4d, 0x6a, 0x49, 0x2d, 0x46, 0x0f, 0x48, 0xb2, 0xd1,
	0x6d, 0x9b, 0x91, 0x64, 0x73, 0x60, 0x21, 0x87, 0x16, 0xbc, 0x02, 0x47, 0x55, 0x85, 0xf0, 0xed,
	0xc0, 0x1a, 0x79, 0xe3, 0xb3, 0xa8, 0xf9, 0x2b, 0x6a, 0xa6, 0x44, 0x71, 0x55, 0x08, 0xa6, 0x05,
	0x88, 0xe0, 0x64, 0x7c, 0x2b, 0x7c, 0x47, 0xdb, 0x75, 0x8d, 0x43, 0xe8, 0xa6, 0xf9, 0x07, 0x57,
	0x49, 0x9e, 0xf9, 0x6d, 0xfd, 0xbe, 0x67, 0x9c, 0x40, 0x77, 0x2b, 0x14, 0xdf, 0x70, 0xc5, 0x7d,
	0x37, 0xb0, 0x47, 0xbd, 0xf1, 0xe5, 0x9f, 0xe1, 0x73, 0xd3, 0xa6, 0x99, 0x92, 0x15, 0xdb, 0xab,
	0x87, 0x77, 0xd0, 0x3f, 0x6a, 0xe1, 0x00, 0xec, 0x4f, 0x51, 0x99, 0xac, 0x75, 0x89, 0xe7, 0xd0,
	0xfe, 0xe2, 0xe9, 0x4e, 0x98, 0x30, 0x0d, 0xdc, 0x92, 0x89, 0x15, 0xce, 0xc0, 0xa9, 0x97, 0xc6,
	0x1e, 0x74, 0x1e, 0xe9, 0x6c, 0xba, 0x7e, 0x8d, 0x07, 0x2d, 0x3c, 0x81, 0x5e, 0x4c, 0xe7, 0x4b,
	0xca, 0xa6, 0xf1, 0x9a, 0xd1, 0x81, 0x85, 0x08, 0xde, 0xf2, 0x79, 0x11, 0x2f, 0x18, 0x5d, 0xbd,
	0xac, 0xf4, 0x1b, 0xc1, 0xff, 0xd0, 0x9d, 0x2f, 0x0c, 0xd9, 0xe3, 0x7b, 0xe8, 0xbc, 0xed, 0x84,
	0x4c, 0x84, 0xc4, 0x1b, 0x38, 0x7d, 0x12, 0xaa, 0x59, 0xfa, 0xa1, 0x32, 0x27, 0xf0, 0x7e, 0xc3,
	0xd4, 0x3c, 0xf4, 0x8e, 0xc3, 0x85, 0xad, 0x77, 0x57, 0x5f, 0xef, 0xfa, 0x27, 0x00, 0x00, 0xff,
	0xff, 0xb2, 0x21, 0x70, 0x5e, 0xce, 0x01, 0x00, 0x00,
}
